package com.kardexcarousel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.kardexcarousel.APIService.APIService;
import com.kardexcarousel.APIService.RetroClient;
import com.kardexcarousel.Alertbox.Alertbox;
import com.kardexcarousel.Model.ArticleResponse;
import com.kardexcarousel.Model.LoginResponse;
import com.kardexcarousel.Model.TrayData;
import com.kardexcarousel.Model.TrayResponse;
import com.kardexcarousel.Network.Network_Connection_Activity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener{
    String[] data = {"Select", "PUT", "PICK"};
    Button scan,scans,submit;
    Spinner spinner;
    Alertbox alertbox;
    String tray,articleno,information,quantity,getuserid;
    int setvalue=0;
    EditText trayno,info,qty,article;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor isRegister;
    int value=0;
    ImageView logout;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        trayno=findViewById(R.id.trayno);
        logout=findViewById(R.id.logout);
        info=findViewById(R.id.info);
        qty=findViewById(R.id.qty);
        article=findViewById(R.id.articleno);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        builder = new AlertDialog.Builder(this);
        editor = myshare.edit();
        scan=findViewById(R.id.scan);
        scans=findViewById(R.id.scans);
        spinner=findViewById(R.id.spinner);
        submit=findViewById(R.id.submit);
        alertbox=   new Alertbox(HomeActivity.this);
    getuserid=myshare.getString("userid","");



       article.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               if (s.length()>6){
                   articleno=article.getText().toString();
                   getInformation();
               }

           }

           @Override
           public void afterTextChanged(Editable s) {
           }
       });

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,data);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);
        spinner.setOnItemSelectedListener(this);


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setMessage("Are you sure want to logout ?");
                builder.setPositiveButton(R.string.yes , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences preferences = getSharedPreferences("Registration", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        finish();
                        Intent intent = new Intent(HomeActivity.this, RegisterActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
        scans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value=0;
                IntentIntegrator intentIntegrator=new IntentIntegrator(HomeActivity.this);
                intentIntegrator.setPrompt("For flash use volume up key");
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setOrientationLocked(true);
                intentIntegrator.setCaptureActivity(Capture.class);
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                intentIntegrator.initiateScan();
            }
        });

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value=1;
                IntentIntegrator intentIntegrator=new IntentIntegrator(HomeActivity.this);
                intentIntegrator.setPrompt("For flash use volume up key");
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setOrientationLocked(true);
                intentIntegrator.setCaptureActivity(Capture.class);
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
              intentIntegrator.initiateScan();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 tray =trayno.getText().toString();
                 articleno=article.getText().toString();
                 information=info.getText().toString();
                 quantity=qty.getText().toString();
                 if (tray.equals("")){
                     alertbox.showAlert("Enter tray no");
                 }
                 else if(articleno.equals("")){
                     alertbox.showAlert("Enter article no");
                 }
                 else if(quantity.equals("")){
                     alertbox.showAlert("Enter quantity");
                 }
                 else  if(information.equals("")){
                     alertbox.showAlert("Enter information");
                 }
                 else{
                     checkinternet();
                 }



            }
        });

    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(HomeActivity.this);

        if (connection.CheckInternet()) {
            getTrackDatas();

        } else {
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult intentResult=IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(intentResult.getContents()  !=null){

          if (value == 0){
              trayno.setText(intentResult.getContents());
          }
          else if(value ==1){
              article.setText(intentResult.getContents());
          }


        }
        else {
            Toast.makeText(getApplicationContext(),"Scan Cancelled",Toast.LENGTH_LONG).show();
        }
    }

    private  void getInformation(){


        final ProgressDialog loading = ProgressDialog.show(HomeActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        APIService apiService = RetroClient.getApiService();

        Call<ArticleResponse> call = apiService.articledata(articleno);
        call.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                try {
                    //if (response.body().getMsg().equals("order desc")) {
                    if (response.body().isResult()) {
                        loading.dismiss();
                        String getinfo = response.body().getData();
                        info.setText(getinfo);
                    }
                else  if(response.body().isResult()) {
                        loading.dismiss();
                        alertbox.showAlert("Invalid code");
                    }
                else{
                        loading.dismiss();
                    }
                } catch (Exception e) {
                    Log.e("test11",e.getMessage());
                    loading.dismiss();
                    e.printStackTrace();
                    alertbox.showAlert("Something went wrong,Please try again later");
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                loading.dismiss();
                alertbox.showAlert("Something went wrong,Please try again later");
            }
        });
    }


    private  void getTrackDatas(){


        final ProgressDialog loading = ProgressDialog.show(HomeActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        APIService apiService = RetroClient.getApiService();

        Call<TrayResponse> call = apiService.scandata(tray,articleno,setvalue,quantity,information,getuserid);
        call.enqueue(new Callback<TrayResponse>() {
            @Override
            public void onResponse(Call<TrayResponse> call, Response<TrayResponse> response) {
                try {
                    if (response.body().getMsg().equals("Scan data Added successfuly")) {
                        loading.dismiss();
               alertbox.showAlert("Uploaded Successfully .");
               trayno.setText("");
               article.setText("");
               qty.setText("");
               info.setText("");

                    }
                    else if (response.body().getMsg().equals("Scan data Failed")) {
                        loading.dismiss();
                alertbox.showAlert("Failed to uploaded");
                    }
                    else{
                        loading.dismiss();

                    }
                } catch (Exception e) {
                    loading.dismiss();
                    e.printStackTrace();
                    alertbox.showAlert("Something went wrong,Please try again later");
                }
            }

            @Override
            public void onFailure(Call<TrayResponse> call, Throwable t) {
                loading.dismiss();
                alertbox.showAlert("Something went wrong,Please try again later");
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (data[position] == "PUT"){
            setvalue=1;
        }
else if(data[position] == "PICK"){
    setvalue=2;
}
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}