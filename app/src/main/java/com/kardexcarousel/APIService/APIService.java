package com.kardexcarousel.APIService;

import com.kardexcarousel.Model.ArticleResponse;
import com.kardexcarousel.Model.LoginResponse;
import com.kardexcarousel.Model.TrayData;
import com.kardexcarousel.Model.TrayResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public  interface APIService {

    @FormUrlEncoded
    @POST("api/values/login")
    public Call<LoginResponse> loginresponse(
            @Field("userName") String username,
            @Field("userPassword") String password
    );

    @FormUrlEncoded
    @POST("api/values/addscandata")
    public Call<TrayResponse> scandata(
            @Field("traynumber") String traynumber,
            @Field("article") String article,
            @Field("ordertype") int ordertype,
            @Field("quantity") String qty,
            @Field("orderinformation") String orderinfo,
            @Field("userid") String userid

    );

    @FormUrlEncoded
    @POST("api/values/getorderdesc")
    public Call<ArticleResponse> articledata(
            @Field("itemcode_var") String articleno
    );

}
