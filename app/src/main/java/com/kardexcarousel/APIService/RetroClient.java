package com.kardexcarousel.APIService;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {

//   public static final String ROOT_URL = "http://192.168.65.180:82/kardexapi/";
   public static final String ROOT_URL = "http://addisonreach.co.in/kardexapi/";
    private static Retrofit getRetrofitInstance() {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(new GsonBuilder()
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static Retrofit getRetrofitQualityInstance() {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(new GsonBuilder()
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
    }


    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }

    public static APIService getApiQualityService() {
        return getRetrofitQualityInstance().create(APIService.class);
    }
}
