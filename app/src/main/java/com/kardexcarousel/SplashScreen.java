package com.kardexcarousel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends AppCompatActivity {
    private SharedPreferences myshare;
    private boolean isregister;
    String islogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        isregister = myshare.getBoolean("isregister", false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent ;


                if (isregister) {
                    mainIntent = new Intent(SplashScreen.this, HomeActivity.class);
                    finish();
                }

//             
                else{
                    mainIntent = new Intent(SplashScreen.this, RegisterActivity.class);
                    finish();
                }
                startActivity(mainIntent);
                finish();

            }
        }, 2000);


    }
}