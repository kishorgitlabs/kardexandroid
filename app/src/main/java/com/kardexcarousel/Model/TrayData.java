package com.kardexcarousel.Model;

import com.google.gson.annotations.SerializedName;

public class TrayData {

	@SerializedName("traynumber")
	private String traynumber;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("orderinformation")
	private String orderinformation;

	@SerializedName("errorreason")
	private Object errorreason;

	@SerializedName("ID")
	private int iD;

	@SerializedName("ordername")
	private String ordername;

	@SerializedName("errordate")
	private Object errordate;

	@SerializedName("userid")
	private String userid;

	@SerializedName("article")
	private String article;

	@SerializedName("status")
	private int status;

	@SerializedName("ordertype")
	private String ordertype;

	public String getTraynumber(){
		return traynumber;
	}

	public String getQuantity(){
		return quantity;
	}

	public String getOrderinformation(){
		return orderinformation;
	}

	public Object getErrorreason(){
		return errorreason;
	}

	public int getID(){
		return iD;
	}

	public String getOrdername(){
		return ordername;
	}

	public Object getErrordate(){
		return errordate;
	}

	public String getUserid(){
		return userid;
	}

	public String getArticle(){
		return article;
	}

	public int getStatus(){
		return status;
	}

	public String getOrdertype(){
		return ordertype;
	}
}