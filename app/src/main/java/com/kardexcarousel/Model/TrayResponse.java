package com.kardexcarousel.Model;

import com.google.gson.annotations.SerializedName;

public class TrayResponse{

	@SerializedName("result")
	private boolean result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private TrayData data;

	public boolean isResult(){
		return result;
	}

	public String getMsg(){
		return msg;
	}

	public TrayData getData(){
		return data;
	}
}