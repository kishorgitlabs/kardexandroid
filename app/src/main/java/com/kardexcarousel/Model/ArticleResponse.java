package com.kardexcarousel.Model;

import com.google.gson.annotations.SerializedName;

public class ArticleResponse{

	@SerializedName("result")
	private boolean result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private String data;

	public boolean isResult(){
		return result;
	}

	public String getMsg(){
		return msg;
	}

	public String getData(){
		return data;
	}
}