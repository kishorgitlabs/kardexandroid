package com.kardexcarousel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kardexcarousel.APIService.APIService;
import com.kardexcarousel.APIService.RetroClient;
import com.kardexcarousel.Alertbox.Alertbox;
import com.kardexcarousel.Model.LoginResponse;
import com.kardexcarousel.Network.Network_Connection_Activity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity {
    Button login_button;
    EditText username,password;
    SharedPreferences myshare, share;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor isRegister;
   String strname,strpassword,emailPattern ;
    Alertbox alertbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
         login_button=findViewById(R.id.login_button);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
    alertbox=   new Alertbox(RegisterActivity.this);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strname=username.getText().toString().trim();
                emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                strpassword=password.getText().toString();
                if (strname.equals("")){
                    username.setFocusable(true);
                    username.setError("Enter your mail");
                }
//                else if(!strname.matches(emailPattern)){
//                    username.setFocusable(true);
//                    username.setError("Invalid email address");
//                }
                else if(strpassword.equals("")){
                    password.setFocusable(true);
                    password.setError("Enter your password");
                }
             else{
                 checkinternet();
                }
            }
        });


    }


    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(RegisterActivity.this);

        if (connection.CheckInternet()) {
     getLogin();

        } else {
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private  void getLogin(){


            final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService apiService = RetroClient.getApiService();

            Call<LoginResponse> call = apiService.loginresponse(strname,strpassword);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        if (response.body().getMsg().equals("Login Successfully")) {
                            loading.dismiss();
                            Intent go = new Intent(RegisterActivity.this, HomeActivity.class);
                            go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(go);
                            finish();
                            isRegister=editor.putBoolean("isregister",true);
                            editor.putString("userid",response.body().getData().getUserID());
                            editor.apply();
                            editor.commit();
                        }
                        else if (response.body().getMsg().equals("Invalid Username or Password")) {
                            loading.dismiss();
                            alertbox.showAlert("Incorrect username or password");
                        }
                    } catch (Exception e) {
                       loading.dismiss();
                        e.printStackTrace();
                        alertbox.showAlert("Something went wrong,Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    loading.dismiss();
        alertbox.showAlert("Something went wrong,Please try again later");
                }
            });
        }


    }
